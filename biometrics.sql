CREATE TABLE IF NOT EXISTS weight (
  date DATE NOT NULL PRIMARY KEY,
  weight TEXT
);

CREATE TABLE IF NOT EXISTS measurements (
  date DATE NOT NULL PRIMARY KEY,
  BodyFat REAL,
  LeanMass REAL
);
