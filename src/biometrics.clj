(ns biometrics
  (:require [clojure.math :as math]
            [next.jdbc :as jdbc]
            [next.jdbc.result-set :as rs]))

;; Database stuff

(def db (get (load-file "config.edn") :biometrics))
(def ds (jdbc/get-datasource db))

(defn bf
  "Calculates body fat percentage using the US Navy Method. Returns percentage as float."
  [waist neck height]
  (+ (- (* 86.010 (math/log10 (- waist neck)))
        (* 70.041 (math/log10 height)))
     36.76))

(defn add-weight
  "Adds today's body weight to biometrics.weight table."
  [weight]
    (jdbc/execute! ds [(str "INSERT INTO weight (date, weight) "
                            "VALUES (CURRENT_DATE, " weight ");")]))

(defn add-measurement
  "Adds today's body weight and body fat to the biometrics.measurements
  and biometrics.weight tables."
  [waist neck height weight]
  (let [BodyFat (math/round (bf waist neck height))
        LeanMass (math/round (- weight (* weight (/ BodyFat 100))))]
    (println (str "Today's Body Fat: " BodyFat "%"))
    (println (str "Today's Lean Mass: " LeanMass))
    (jdbc/execute! ds [(str "INSERT INTO measurements (date, BodyFat, LeanMass)"
                            "VALUES (CURRENT_DATE, " BodyFat ", " LeanMass ");")])
    (add-weight weight)))

(defn weight-hist []
  (jdbc/execute! ds ["SELECT * FROM weight;"] {:builder-fn rs/as-unqualified-maps}))

(defn measurement-hist []
  (jdbc/execute! ds ["SELECT * FROM measurements;"] {:builder-fn rs/as-unqualified-maps}))

(println)
(println "Weight History:")
(println (weight-hist))
(println)
(println "Measurement History: ")
(println (measurement-hist))
